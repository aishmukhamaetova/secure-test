import React from 'react'
import Head from 'next/head'

export default function Home() {
  const [value, setValue] = React.useState(null)

  async function onQuery() {
    const r = await fetch(`/api`)
    const {value} = await r.json()
    setValue(value)
  }

  async function onForget() {
    fetch(`/api/forget`)
    setValue(null)
  }

  const text = value !== null ? value : `Click fetch`

  return (
    <div className="container">
      <Head>
        <title>Secure test application</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>Title ({text})</h1>
        <button onClick={onQuery}>Fetch</button>
        <button onClick={onForget}>Forget session</button>
      </main>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}
