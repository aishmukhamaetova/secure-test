const run = require('./app')
const http = require('http')

const dev = process.env.NODE_ENV !== `production`

const defaults = {
  port: dev ? 4000 : 80
}

const PORT = process.env.PORT || defaults.port

const debug = (...args) => console.log(...args)

let server

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof port === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

function onListening() {
  const addr = server.address()
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  debug('Listening on ' + bind)
}

async function main() {
  const app = await run()

  app.set('port', PORT)

  server = http.createServer(app)

  server.listen(PORT)
  server.on('error', onError)
  server.on('listening', onListening)
}

main()
