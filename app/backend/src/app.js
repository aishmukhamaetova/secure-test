const cors = require(`cors`)
const express = require(`express`)
const cookieParser = require(`cookie-parser`)

const session = require(`express-session`)
const FileStore = require(`session-file-store`)(session)

async function process(req, res) {
  const {value = 0} = req.session
  req.session.value = value + 1

  res.json({r: Date.now(), value})
}

async function forget(req, res) {
  req.session.destroy()

  res.json({r: Date.now()})
}

async function run() {
  const app = express()

  app.use(cors())

  app.use(express.json())

  app.use(express.urlencoded({extended: false}))
  app.use(cookieParser())

  app.set(`trust proxy`, 1)
  app.use(
    session({
      secret: `secret`,
      resave: true,
      saveUninitialized: true,
      cookie: {secure: true},
      proxy: true,
      unset: `keep`,
      store: new FileStore({})
    })
  )

  app.get(`/api`, process)
  app.get(`/api/forget`, forget)

  return app
}

module.exports = run
